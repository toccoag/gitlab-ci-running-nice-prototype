# GitLab CI - Running Nice with an Empty DB

A scheduled jobs creates [Nice][] and a [Postgres][] Docker images daily for every release
branch and master. The Postgres image contains a empty DB fitting to the corresponding Nice
image.

In addition to running all changesets, a user named 'tocco-test' is created
and granted all permissions.

This sample project does …

1. … start Nice and Postgres as service
2. … wait for Nice to be become ready
3. … a simple REST request as user 'tocco-test'

See also comments in [.gitlab-ci.yml](.gitlab-ci.yml).

Secrets can be found as [Variables]. Additionally, credentials for
user 'tocco-test' are in [secrets2.yml]. (Remember that protected
variables (the default) are only visibale in protected branches.)

[Nice]: https://gitlab.com/toccoag/nice2/container_registry/2866479
[Postgres]:  https://gitlab.com/toccoag/nice2/container_registry/2866474
[Variables]: https://gitlab.com/toccoag/gitlab-ci-running-nice-prototype/-/settings/ci_cd#Variables
[secrets2.yml]: https://docs.tocco.ch/glossary.html#term-secrets2.yml
